<?php

namespace Totoro1302\Captcha;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Mockery\Matcher\Closure;

class CaptchaServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('captcha.php')
        ]);

        Validator::extend('captcha', \Closure::fromCallable([$this->app->make(CaptchaContract::class), 'isHuman']));
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'captcha');

        // Binding CaptchaContract to singleton implementation GoogleReCaptcha
        $this->app->singleton(CaptchaContract::class, function($app){
            return new GoogleReCaptcha(
                $app->make('config')->get('captcha.recaptcha_sitekey'),
                $app->make('config')->get('captcha.recaptcha_secretkey'),
                $app->make('request')->ip()
            );
        });

    }
}
