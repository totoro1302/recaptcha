<?php

namespace Totoro1302\Captcha;

use \Illuminate\Validation\Validator;

class GoogleReCaptcha implements CaptchaContract
{
    protected const GOOGLE_RECAPTCHA_JSAPI = 'https://www.google.com/recaptcha/api.js';

    protected const GOOGLE_RECAPTCHA_VERIFY_URL = 'https://www.google.com/recaptcha/api/siteverify';

    protected const GOOGLE_RECAPTCHA_WIDGET = '<div class="%s" data-sitekey="%s"></div>';

    protected const GOOGLE_RECAPTCHA_WIDGET_CSSCLASS = 'g-recaptcha';

    protected $_siteKey = '';

    protected $_privateKey = '';

    protected $_clientIpAddress = '';

    public function __construct(string $siteKey, string $privateKey, ?string $clientIpAddress = null) {
        $this->_siteKey = $siteKey;
        $this->_privateKey = $privateKey;
        $this->_clientIpAddress = $clientIpAddress;
    }

    public function getGoogleRecaptchaJsApi(): string {
        return self::GOOGLE_RECAPTCHA_JSAPI;
    }

    public function getGoogleRecaptchaSiteKey(): string {
        return $this->_siteKey;
    }

    public function getWidget(): string {
        return sprintf(self::GOOGLE_RECAPTCHA_WIDGET, self::GOOGLE_RECAPTCHA_WIDGET_CSSCLASS, $this->_siteKey);
    }

    public function isHuman($attribute, string $value = null, ?array $parameters = null, ?Validator $validator = null): bool {

        if($value === null){
            return false;
        }

        $postData = [
            'secret' => $this->_privateKey,
            'response' => $value,
        ];

        if($this->_clientIpAddress !== null){
            $postData['remoteip'] = $this->_clientIpAddress;
        }

        if(function_exists('curl_version')){
            $response = $this->checkWithCurl($postData);
        } else {
            $response = $this->checkWithFileGetContent($postData);
        }

        if($response->success === false){
            foreach($response->{'error-codes'} as $error){
                $validator->errors()->add('g-recaptcha-response', $error);
            }
        }

        return $response->success;
    }
    
    protected function checkWithCurl(array $postData) {

        $ch = curl_init(self::GOOGLE_RECAPTCHA_VERIFY_URL);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response);
    }

    protected function checkWithFileGetContent(array $postData) {

        $streamOptions = [
            'http' => [
                'header' => '',
                'method' => 'POST',
                'content' => http_build_query($postData)
            ]
        ];

        $context = stream_context_create($streamOptions);
        $content = file_get_contents(self::GOOGLE_RECAPTCHA_VERIFY_URL, false, $context);

        return json_decode($content);
    }
}