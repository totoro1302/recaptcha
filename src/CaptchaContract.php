<?php

namespace Totoro1302\Captcha;

use \Illuminate\Validation\Validator;

interface CaptchaContract {

    /**
     * Return the captcha widget
     */
    public function getWidget(): string;

    /**
     *
     * Check whether or not the captcha is valid
     *
     * @param $attribute
     * @param string|null $value
     * @param array|null $parameters
     * @param null|Validator $validator
     * @return bool
     */
    public function isHuman($attribute, string $value = null, ?array $parameters = null, ?Validator $validator = null): bool;
}